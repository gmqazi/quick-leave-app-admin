package com.example.quickleaveappadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddClass extends AppCompatActivity {

    EditText txtclass;
    Button btnaddclass;

    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);

        txtclass = findViewById(R.id.txtclass);
        btnaddclass = findViewById(R.id.btnaddclass);

        databaseReference = FirebaseDatabase.getInstance().getReference("Classes");

        btnaddclass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtclasses = txtclass.getText().toString().trim();

                if (!TextUtils.isEmpty(txtclasses))
                {
                    String classesid = databaseReference.push().getKey();

                    Classes classes =new Classes(classesid,txtclasses);

                    databaseReference.child(classesid).setValue(classes);

                    Toast.makeText(AddClass.this, "Class Inserted Successfully",Toast.LENGTH_LONG).show();

                    startActivity(new Intent(AddClass.this,ClassesActivity.class));
                }

                else
                {
                    Toast.makeText(AddClass.this,"Enter Class Correctly",Toast.LENGTH_LONG).show();

                }

            }
        });



    }
}