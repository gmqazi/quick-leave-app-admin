package com.example.quickleaveappadmin;

public class Classes {

    String classid;
    String classname;

    public Classes() {
    }

    public Classes(String classid, String classname) {
        this.classid = classid;
        this.classname = classname;
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }
}
