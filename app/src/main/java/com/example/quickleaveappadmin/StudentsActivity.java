package com.example.quickleaveappadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class StudentsActivity extends AppCompatActivity {

Button addstudent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);

        String classidss = getIntent().getStringExtra("classid");
        addstudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addstd = new Intent(StudentsActivity.this,AddStudent.class);
                startActivity(addstd);
            }
        });


    }
}