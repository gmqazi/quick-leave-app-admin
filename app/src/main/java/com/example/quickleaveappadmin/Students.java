package com.example.quickleaveappadmin;

public class Students {
    String studentid;
    String stdname;
    String rollno;
    String present;
    String absent;
    String leave;

    public Students() {
    }

    public Students(String studentid, String stdname, String rollno, String present, String absent, String leave) {
        this.studentid = studentid;
        this.stdname = stdname;
        this.rollno = rollno;
        this.present = present;
        this.absent = absent;
        this.leave = leave;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getStdname() {
        return stdname;
    }

    public void setStdname(String stdname) {
        this.stdname = stdname;
    }

    public String getRollno() {
        return rollno;
    }

    public void setRollno(String rollno) {
        this.rollno = rollno;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public String getAbsent() {
        return absent;
    }

    public void setAbsent(String absent) {
        this.absent = absent;
    }

    public String getLeave() {
        return leave;
    }

    public void setLeave(String leave) {
        this.leave = leave;
    }
}
