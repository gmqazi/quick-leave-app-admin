package com.example.quickleaveappadmin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ClassesActivity extends AppCompatActivity {

    Button btnAddClass;
    ListView classeslist;

    List<Classes> listclasses;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes);


        classeslist = findViewById(R.id.classeslist);

        listclasses = new ArrayList<>();
        databaseReference = FirebaseDatabase.getInstance().getReference("Classes");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot userssnapshot:snapshot.getChildren()){
                    Classes classs = userssnapshot.getValue(Classes.class);
                    listclasses.add(classs);
                }

                ListAdapter adapter = new ListAdapter(ClassesActivity.this,listclasses);
                classeslist.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        classeslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Classes classs = listclasses.get(position);
                Intent intent = new Intent(ClassesActivity.this,StudentsActivity.class);
                intent.putExtra("classid",classs.classid);
                startActivity(intent);
            }
        });

        btnAddClass = findViewById(R.id.btnAddClass);
        btnAddClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ClassesActivity.this, AddClass.class));
            }
        });



    }
        }