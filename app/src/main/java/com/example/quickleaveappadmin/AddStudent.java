package com.example.quickleaveappadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class AddStudent extends AppCompatActivity {

    EditText txtstdname,txtrollnos;
    Button btnaddstudent;

    List<Classes> listclass;

    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        txtstdname = findViewById(R.id.txtstdname);
        txtrollnos = findViewById(R.id.txtrollno);

        btnaddstudent = findViewById(R.id.btnaddstudent);

        databaseReference = FirebaseDatabase.getInstance().getReference("Students");

        listclass = new ArrayList<>();

        btnaddstudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txtstudentname = txtstdname.getText().toString().trim();
                String txtrollno = txtrollnos.getText().toString().trim();
                String Present = "0";
                String Absent = "0";
                String Leave = "0";
                Classes classes = new Classes();

                if (TextUtils.isEmpty(txtstudentname))
                {
                    Toast.makeText(AddStudent.this,"Enter Class Correctly",Toast.LENGTH_LONG).show();
                }

                else
                {
                    String studentid = databaseReference.push().getKey();

                    Students students =new Students(studentid,txtstudentname,txtrollno,Present,Absent,Leave);

                    databaseReference.child(classes.classid).child(studentid).setValue(students);

                    Toast.makeText(AddStudent.this, "Student Inserted Successfully",Toast.LENGTH_LONG).show();

                    startActivity(new Intent(AddStudent.this,StudentsActivity.class));


                }

            }
        });
    }
}