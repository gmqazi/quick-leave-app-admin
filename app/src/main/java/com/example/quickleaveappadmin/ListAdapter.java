package com.example.quickleaveappadmin;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class ListAdapter extends ArrayAdapter {

    private Activity mContext;
    private List<Classes> listclasses;
    public ListAdapter(Activity mContext, List<Classes> listclasses){
        super(mContext,R.layout.show_classname,listclasses);

        this.mContext = mContext;
        this.listclasses = listclasses;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = mContext.getLayoutInflater();
        View ListItemView = inflater.inflate(R.layout.show_classname,null,true);
        TextView txUsername = ListItemView.findViewById(R.id.classname);


        Classes classes = listclasses.get(position);
        txUsername.setText(classes.classname);

        return ListItemView;
    }
}
